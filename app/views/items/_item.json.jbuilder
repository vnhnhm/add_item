json.extract! item, :id, :name, :value, :created_at, :updated_at
json.url item_url(item, format: :json)