Rails.application.routes.draw do
  resources :items
  resources :books
  resources :cars
  get 'page/index'
  root to: 'cars', to: 'cars#show', id: 2
  get 'add_item', to: 'items#add_item'
end
